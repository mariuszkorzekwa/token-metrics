var Web3 = require('web3');
const {getContracts, getAllTokensInfo} = require('./utils');
var constants = require('../constants');

let web3 = new Web3(new Web3.providers.HttpProvider(constants.INFURA_BLOCKCHAIN_URL));
let contracts = getContracts(web3, {...constants.ADDRESSES_OF_TOKENS});
let allTokensInfo = getAllTokensInfo(contracts);

module.exports = {
  allTokensInfo,
  isConnected: web3.isConnected()
};
