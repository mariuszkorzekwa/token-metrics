var Web3 = require('web3');
let web3 = new Web3(new Web3.providers.HttpProvider());

let {getContracts} = require('./utils');

test('get contract web3 objects', () => {
  let addresses = {
    BNB: '0xB8c77482e45F1F44dE1745F52C74426C631bDD52',
    VEN: '0xd850942ef8811f2a866692a623011bde52a462c1'
  };
  let contracts = getContracts(web3, {...addresses});
  expect(Object.keys(contracts).length).toBe(2);
  expect(contracts['BNB'].address).toBe(addresses['BNB']);
  expect(contracts['VEN'].address).toBe(addresses['VEN']);
});