import React, {Component} from 'react';
import withRoot from './withRoot';
import Tokens from './components/Tokens/index'

class App extends Component {
  render() {
    return (<Tokens/>)
  }
}

export default withRoot(App);
