import {tokensTypes} from '../../constants/actionTypes'

const initialState = {
  tokens: [],
  isLoading: true,
  isError: false
};

export default (state = initialState, action) => {
  switch (action.type) {
    case tokensTypes.GET_TOKENS_REQUESTED:
      return {...state, isLoading: true};
    case tokensTypes.GET_TOKENS_DONE:
      return {...state, isLoading: false, tokens: action.payload};
    case tokensTypes.GET_TOKENS_FAILED:
      return {...state, isLoading: false, isError: true};
    default:
      return state;
  }
};
