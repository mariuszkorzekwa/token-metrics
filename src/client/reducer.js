import {combineReducers} from 'redux';
import tokens from './components/Tokens/reducer'

export default combineReducers({
  tokens,
});
