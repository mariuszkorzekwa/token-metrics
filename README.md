Token metrics
=============

Localhost install (without docker, with python virtualenv)
----------------------------------------------------------
* Create python virtualenv and activate it.
* Install nodeenv `pip install nodeenv`
* Install node by nodeenv `nodeenv -p --prebuilt --node=10.9.0`


Docker: developing (with hot reloading)
---------------------------------------
* Create new `.env` file based on `.env.default` and add envs.
* Install node requirements `yarn install`
* Build local image `docker-compose build`
* Run containers `docker-compose up`

Docker: gitlabci
----------------
* To build and push gitlabci image run below commands
* `docker-compose -f docker-compose.gitlabci.yml build`
* `docker-compose -f docker-compose.gitlabci.yml push`
