FROM node:10.9-stretch

ENV APP_ROOT=/opt/app

RUN mkdir \
    $APP_ROOT

COPY . $APP_ROOT
WORKDIR $APP_ROOT
